import sqlite3


class Select_Data:

    @staticmethod
    def select_stocks(compani):
        connection = sqlite3.connect('data.db')
        cursor = connection.cursor()
        sqlite_select_query = f"""SELECT * FROM Stocks WHERE name = '{str(compani)}' """
        cursor.execute(sqlite_select_query)
        records = cursor.fetchall()
        cursor.close()
        connection.close()
        if len(records) != 0:
            return records
        else:
            return False

    @staticmethod
    def all_select_stocks():
        connection = sqlite3.connect('data.db')
        cursor = connection.cursor()
        sqlite_select_query = """SELECT name FROM Stocks """
        cursor.execute(sqlite_select_query)
        records = cursor.fetchall()
        cursor.close()
        connection.close()
        return records

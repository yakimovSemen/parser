import requests
from models.model_bonds import Values
from settings import Settings
from saves import Save_Data


class Bonds_Parse:
    def __init__(self, url):
        self.url = url #---------
        self.proxies = Settings.proxies
        self.cookies = Settings.cookies_stocks
        self.headers = Settings.headers_stocks
        self.params = Settings.params_stocks
        # кол-во бумаг
        self.quantity_paper = Settings.quantity_paper(960)

    def parse(self):
        i = 0  # страницы
        startset = 0  # первая на странице
        endset = 12  # последняя на странице
        # парсит все полные страницы ps: полные считаются страницы с 12 бумагами
        while endset <= self.quantity_paper[0]:
            response = requests.post(#--------
                self.url,
                params=self.params,
                cookies=self.cookies,
                headers=self.headers,
                json=Settings.json_data_stocks(startset=startset, endset=endset),
                proxies=self.proxies,
            )

            item = Values.parse_obj(response.json()['payload']) #--------
            Save_Data.save_stocks(item) #--------
            startset += 12
            endset += 12
            i += 1
            print(item)
            print(f"CПАРСИЛ ОБЛИГАЦИИ С {i} CТРАНИЦЫ")
            # print(response.json())

        # парсит последнюю не полную страницу (может быть пустой)
        endset = startset + self.quantity_paper[1]  # последняя на странице
        num_paper = self.quantity_paper[0] + self.quantity_paper[1]
        if endset <= num_paper:
            response = requests.post(
                self.url,
                params=self.params,
                cookies=self.cookies,
                headers=self.headers,
                json=Settings.json_data_stocks(startset=startset, endset=endset),
                proxies=self.proxies,
            )
            item = Values.parse_obj(response.json()['payload'])
            Save_Data.save_stocks(item)
            i += 1
            print(item)
            print(f"CПАРСИЛ С {i} CТРАНИЦЫ")

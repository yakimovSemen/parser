class Settings:
    proxies = {
        'http': 'http://45.11.95.165:6005'
    }

    # Метод для отделения полных и не полных страниц ps: полные считаются страницы с 12 бумагами
    @staticmethod
    def quantity_paper(num_paper: int):
        if num_paper % 12 == 0:
            num_remains = 0
            return_list_value = [num_paper, num_remains]
        else:
            num_remains = num_paper % 12
            num_paper = num_paper - num_remains
            return_list_value = [num_paper, num_remains]
        # кол-во бумаг, остаток от 12
        return return_list_value

    # Акции
    url_stocks = 'https://www.tinkoff.ru/api/trading/stocks/list'
    cookies_stocks = {
        'stDeIdU': '6bea93bb-9594-4902-a7a3-94d2b488ad48',
        '__P__wuid': '1811113845f5206afae1a7ba3f6fbc8a',
        'dco.id': '77929123-63e7-406a-8451-00002043623d',
        'timezone': 'Asia/Yekaterinburg',
        'userType': 'Visitor',
        'dsp_click_id': 'no%20dsp_click_id',
        'utm_source': 'yandex.ru',
        'pageLanding': 'https%3A%2F%2Fwww.tinkoff.ru%2Finvest%2F',
        'isStandalone': 'false',
        'tmr_lvid': 'eec7e224ef73c9c6fe1d568973eb58a0',
        'tmr_lvidTS': '1707663415627',
        '_ym_uid': '1707663416580679130',
        '_ym_d': '1707663416',
        'utm_date_set': '1708020988858',
        '__P__wuid_last_update_time': '1708020988861',
        'investpublicPsid': 'mnVwcy7ysG2uQypOmdHhV9i5e3ACGJJ2.ix-prod-api41',
        '_ym_isad': '2',
        '_t_modern': 'null',
        '__P__wuid_visit_id': 'v1%3A0000008%3A1708024576995%3A1811113845f5206afae1a7ba3f6fbc8a',
        '__P__wuid_visit_persistence': '1708024576995',
        'vIdUid': '27fb80a1-27c3-4041-97af-4d08566e8901',
        'stSeStTi': '1708024605905',
        'invest_chart_type': '%22area%22',
        'invest_chart_candles_duration': '%224hours%22',
        'api_session_csrf_token_4e364a': '4e85a43d-a58e-4327-ab83-742f0e1872b8.1708025552',
        'api_session': '6K6tToQDOGsmeR5Pj22Bq68fgoU1FRn4.ix-prod-api41',
        'api_session_csrf_token_01dd03': '352af4dc-8915-4855-9c32-c7fc3bba1cdd.1708025644',
        'api_session_csrf_token_86f676': '79584f6a-6e2c-477a-8fd5-ba8cf8e9f638.1708026133',
        'psid': 'vRdfWYqJKoQpvCzHKJllAQkkBixzdQpg.ix-prod-api41',
        'api_session_csrf_token_debaf8': '94d4c306-00ca-442b-9b72-8f230445dbbd.1708026603',
        'api_session_csrf_token_0dcf44': '539fd8b9-1de4-4ab0-84c0-e2d7a9f37522.1708026609',
        'tmr_detect': '1%7C1708026609816',
        'mediaInfo': '{%22width%22:838%2C%22height%22:794%2C%22isTouch%22:false%2C%22displayMode%22:%22browser%22%2C%22retina%22:false}',
        'tmr_reqNum': '242',
        'api_session_csrf_token_2304e2': 'abe2a2d5-041e-4a55-9140-dfdee72292b7.1708026632',
        'stLaEvTi': '1708026632300',
    }
    headers_stocks = {
        'authority': 'www.tinkoff.ru',
        'accept': '*/*',
        'accept-language': 'ru-RU,ru;q=0.9,en-US;q=0.8,en;q=0.7',
        'content-type': 'application/json',
        # 'cookie': 'stDeIdU=6bea93bb-9594-4902-a7a3-94d2b488ad48; __P__wuid=1811113845f5206afae1a7ba3f6fbc8a; dco.id=77929123-63e7-406a-8451-00002043623d; timezone=Asia/Yekaterinburg; userType=Visitor; dsp_click_id=no%20dsp_click_id; utm_source=yandex.ru; pageLanding=https%3A%2F%2Fwww.tinkoff.ru%2Finvest%2F; isStandalone=false; tmr_lvid=eec7e224ef73c9c6fe1d568973eb58a0; tmr_lvidTS=1707663415627; _ym_uid=1707663416580679130; _ym_d=1707663416; utm_date_set=1708020988858; __P__wuid_last_update_time=1708020988861; investpublicPsid=mnVwcy7ysG2uQypOmdHhV9i5e3ACGJJ2.ix-prod-api41; _ym_isad=2; _t_modern=null; __P__wuid_visit_id=v1%3A0000008%3A1708024576995%3A1811113845f5206afae1a7ba3f6fbc8a; __P__wuid_visit_persistence=1708024576995; vIdUid=27fb80a1-27c3-4041-97af-4d08566e8901; stSeStTi=1708024605905; invest_chart_type=%22area%22; invest_chart_candles_duration=%224hours%22; api_session_csrf_token_4e364a=4e85a43d-a58e-4327-ab83-742f0e1872b8.1708025552; api_session=6K6tToQDOGsmeR5Pj22Bq68fgoU1FRn4.ix-prod-api41; api_session_csrf_token_01dd03=352af4dc-8915-4855-9c32-c7fc3bba1cdd.1708025644; api_session_csrf_token_86f676=79584f6a-6e2c-477a-8fd5-ba8cf8e9f638.1708026133; psid=vRdfWYqJKoQpvCzHKJllAQkkBixzdQpg.ix-prod-api41; api_session_csrf_token_debaf8=94d4c306-00ca-442b-9b72-8f230445dbbd.1708026603; api_session_csrf_token_0dcf44=539fd8b9-1de4-4ab0-84c0-e2d7a9f37522.1708026609; tmr_detect=1%7C1708026609816; mediaInfo={%22width%22:838%2C%22height%22:794%2C%22isTouch%22:false%2C%22displayMode%22:%22browser%22%2C%22retina%22:false}; tmr_reqNum=242; api_session_csrf_token_2304e2=abe2a2d5-041e-4a55-9140-dfdee72292b7.1708026632; stLaEvTi=1708026632300',
        'origin': 'https://www.tinkoff.ru',
        'referer': 'https://www.tinkoff.ru/invest/stocks/?start=0&end=12&orderType=Desc&sortType=ByPopularity&currencies=RUB',
        'sec-ch-ua': '"Not_A Brand";v="8", "Chromium";v="120", "Opera GX";v="106"',
        'sec-ch-ua-arch': '"x86"',
        'sec-ch-ua-mobile': '?0',
        'sec-ch-ua-model': '""',
        'sec-ch-ua-platform': '"Windows"',
        'sec-ch-ua-platform-version': '"10.0.0"',
        'sec-fetch-dest': 'empty',
        'sec-fetch-mode': 'cors',
        'sec-fetch-site': 'same-origin',
        'traceparent': '00-a2d4744c6cc5177b163b81fb13c93e9a-f20702da1364d5d7-00',
        'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/120.0.0.0 Safari/537.36 OPR/106.0.0.0 (Edition Yx GX)',
    }
    params_stocks = {
        'sessionId': 'vRdfWYqJKoQpvCzHKJllAQkkBixzdQpg.ix-prod-api41',
        'appName': 'web',
        'appVersion': '1.397.0',
        'origin': 'web',
    }

    @staticmethod
    def json_data_stocks(startset: int, endset: int):
        # поместил в метод потому, что надо менять страницы
        json_data = {
            'start': startset,
            'end': endset,
            'orderType': 'Desc',
            'sortType': 'ByPopularity',
            'currencies': ['RUB', ],
            'country': 'All',
            'isPrivate': False,
            'filterOTC': False,
        }
        return json_data

    # Облигации
    url_bonds = 'https://www.tinkoff.ru/api/trading/bonds/list'
    cookies_bonds = {
        '__P__wuid': '1811113845f5206afae1a7ba3f6fbc8a',
        'dco.id': '77929123-63e7-406a-8451-00002043623d',
        'userType': 'Visitor',
        'dsp_click_id': 'no%20dsp_click_id',
        'isStandalone': 'false',
        'tmr_lvid': 'eec7e224ef73c9c6fe1d568973eb58a0',
        'tmr_lvidTS': '1707663415627',
        '_ym_uid': '1707663416580679130',
        '_ym_d': '1707663416',
        'stDeIdU': '1811113845f5206afae1a7ba3f6fbc8a',
        'utm_date_set': '1709655959934',
        'utm_source': 'yandex',
        'utm_content': 'pid%7C50643264312%7Cretid%7C50643264312%7Ccid%7C107704286%7Cgid%7C5406903007%7Caid%7C15868603787%7Cpostype%7Cpremium%7Cpos%7C1%7C%7Csrc%7Cnone%7Cdvc%7Cdesktop%7Cregionid%7C11114',
        'utm_campaign': 'fintech.scholarship_fintech.stip_search_ya_brand_n_n_rus',
        'utm_medium': 'ctx.cpc',
        'utm_term': '%D1%82%D0%B8%D0%BD%D1%8C%D0%BA%D0%BE%D1%84%D1%84%20%D1%81%D1%82%D0%B0%D0%B6%D0%B8%D1%80%D0%BE%D0%B2%D0%BA%D0%B0',
        '_t_modern': 'null',
        'timezone': 'Asia/Yekaterinburg',
        'pageLanding': 'https%3A%2F%2Fwww.tinkoff.ru%2Finvest%2F',
        '__P__wuid_visit_id': 'v1%3A0000026%3A1710870881229%3A1811113845f5206afae1a7ba3f6fbc8a',
        '__P__wuid_visit_persistence': '1710870881229',
        'api_session_csrf_token_ec340b': 'f194cdda-5502-41d2-abe6-d0f691efdaec.1710870881',
        'api_session': 'TDz3fPkVMMkI9ZkVfC7ccHDrHz5Jlk6F.ix-prod-api41',
        '__P__wuid_last_update_time': '1710870881227',
        'api_session_csrf_token_3d3098': '8ea34105-fd45-4a56-9a5a-5ff824864778.1710870881',
        'investpublicPsid': 'sRQuqE3qWhIWuRO590mOV8Zv22tF8OXm.ix-prod-api41',
        'psid': 'XlTRrNmwLBoH1BJaJxMVJb0HZJSvl4og.ix-prod-api41',
        'vIdUid': 'c64989e2-216a-4eae-9781-9e9c0438cb06',
        'stSeStTi': '1710870881881',
        '_ym_isad': '2',
        'api_session_csrf_token_865b97': 'e18800a9-a2ea-4e76-859c-4287f1953427.1710870889',
        'api_session_csrf_token_6ab2aa': '25a85f73-12ad-4e93-a01c-45c70af0e4a5.1710870915',
        'tmr_detect': '0%7C1710870918595',
        'mediaInfo': '{%22width%22:1072%2C%22height%22:794%2C%22isTouch%22:false%2C%22displayMode%22:%22browser%22%2C%22retina%22:false}',
        'tmr_reqNum': '741',
        'api_session_csrf_token_2c78e7': '275dadc9-dbc2-4b4d-8de1-1d4b22354372.1710871220',
        'stLaEvTi': '1710871220925',
    }
    headers_bonds = {
        'authority': 'www.tinkoff.ru',
        'accept': '*/*',
        'accept-language': 'ru-RU,ru;q=0.9,en-US;q=0.8,en;q=0.7',
        'content-type': 'application/json',
        # 'cookie': '__P__wuid=1811113845f5206afae1a7ba3f6fbc8a; dco.id=77929123-63e7-406a-8451-00002043623d; userType=Visitor; dsp_click_id=no%20dsp_click_id; isStandalone=false; tmr_lvid=eec7e224ef73c9c6fe1d568973eb58a0; tmr_lvidTS=1707663415627; _ym_uid=1707663416580679130; _ym_d=1707663416; stDeIdU=1811113845f5206afae1a7ba3f6fbc8a; utm_date_set=1709655959934; utm_source=yandex; utm_content=pid%7C50643264312%7Cretid%7C50643264312%7Ccid%7C107704286%7Cgid%7C5406903007%7Caid%7C15868603787%7Cpostype%7Cpremium%7Cpos%7C1%7C%7Csrc%7Cnone%7Cdvc%7Cdesktop%7Cregionid%7C11114; utm_campaign=fintech.scholarship_fintech.stip_search_ya_brand_n_n_rus; utm_medium=ctx.cpc; utm_term=%D1%82%D0%B8%D0%BD%D1%8C%D0%BA%D0%BE%D1%84%D1%84%20%D1%81%D1%82%D0%B0%D0%B6%D0%B8%D1%80%D0%BE%D0%B2%D0%BA%D0%B0; _t_modern=null; timezone=Asia/Yekaterinburg; pageLanding=https%3A%2F%2Fwww.tinkoff.ru%2Finvest%2F; __P__wuid_visit_id=v1%3A0000026%3A1710870881229%3A1811113845f5206afae1a7ba3f6fbc8a; __P__wuid_visit_persistence=1710870881229; api_session_csrf_token_ec340b=f194cdda-5502-41d2-abe6-d0f691efdaec.1710870881; api_session=TDz3fPkVMMkI9ZkVfC7ccHDrHz5Jlk6F.ix-prod-api41; __P__wuid_last_update_time=1710870881227; api_session_csrf_token_3d3098=8ea34105-fd45-4a56-9a5a-5ff824864778.1710870881; investpublicPsid=sRQuqE3qWhIWuRO590mOV8Zv22tF8OXm.ix-prod-api41; psid=XlTRrNmwLBoH1BJaJxMVJb0HZJSvl4og.ix-prod-api41; vIdUid=c64989e2-216a-4eae-9781-9e9c0438cb06; stSeStTi=1710870881881; _ym_isad=2; api_session_csrf_token_865b97=e18800a9-a2ea-4e76-859c-4287f1953427.1710870889; api_session_csrf_token_6ab2aa=25a85f73-12ad-4e93-a01c-45c70af0e4a5.1710870915; tmr_detect=0%7C1710870918595; mediaInfo={%22width%22:1072%2C%22height%22:794%2C%22isTouch%22:false%2C%22displayMode%22:%22browser%22%2C%22retina%22:false}; tmr_reqNum=741; api_session_csrf_token_2c78e7=275dadc9-dbc2-4b4d-8de1-1d4b22354372.1710871220; stLaEvTi=1710871220925',
        'origin': 'https://www.tinkoff.ru',
        'referer': 'https://www.tinkoff.ru/invest/bonds/?start=0&end=12&country=Russian&orderType=Desc&sortType=ByYieldToClient',
        'sec-ch-ua': '"Not A(Brand";v="99", "Opera GX";v="107", "Chromium";v="121"',
        'sec-ch-ua-arch': '"x86"',
        'sec-ch-ua-mobile': '?0',
        'sec-ch-ua-model': '""',
        'sec-ch-ua-platform': '"Windows"',
        'sec-ch-ua-platform-version': '"10.0.0"',
        'sec-fetch-dest': 'empty',
        'sec-fetch-mode': 'cors',
        'sec-fetch-site': 'same-origin',
        'traceparent': '00-ce0f054e377ff84d4fd5568603e1b3fc-69814dbf15e86282-00',
        'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36 OPR/107.0.0.0 (Edition Yx GX)',
    }
    params_bonds = {
        'sessionId': 'XlTRrNmwLBoH1BJaJxMVJb0HZJSvl4og.ix-prod-api41',
        'appName': 'web',
        'appVersion': '1.406.0',
        'origin': 'web',
    }

    @staticmethod
    def json_data_bonds(startset: int, endset: int):
        # поместил в метод потому, что надо менять страницы
        json_data = {
            'start': startset,
            'end': endset,
            'country': 'Russian',
            'orderType': 'Desc',
            'sortType': 'ByYieldToClient',
            'isPrivate': False,
            'filterOTC': False,
        }
        return json_data

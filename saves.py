import sqlite3


class Save_Data:
    @staticmethod
    def save_stocks(item):
        product_list = []
        # страница
        for product in item.values:
            product_list.append([product.symbol.ticker, product.symbol.showName,
                                 product.price.value, product.lotPrice.value,
                                 product.historicalPrices[0].earningsInfo.relative * 100,
                                 product.historicalPrices[0].earningsInfo.absolute.value,
                                 product.earnings.absolute.value, product.earnings.relative
                                 ])
        connection = sqlite3.connect('data.db')
        cursor = connection.cursor()

        # Создаем таблицу Users
        cursor.execute('''
        CREATE TABLE IF NOT EXISTS Stocks (
        token TEXT NOT NULL PRIMARY KEY,
        name TEXT NOT NULL,
        price TEXT NOT NULL,
        lot_price REAL,
        percent_year REAL,
        price_boost_year  REAL,
        price_boost_day REAL,
        percent_day REAL
        )
        ''')
        # ID = TOKEN
        for i in range(len(product_list)):
            cursor.execute('''INSERT INTO Stocks (token, name, price, lot_price,
                            percent_year, price_boost_year, price_boost_day, percent_day) 
                            VALUES (?, ?, ?, ?, ?, ?, ?, ?) 
                            ON CONFLICT (token)
                            DO UPDATE SET 
                            name = excluded.name, 
                            price = excluded.price, 
                            lot_price = excluded.lot_price, 
                            percent_year = excluded.percent_year, 
                            price_boost_year = excluded.price_boost_year, 
                            price_boost_day = excluded.price_boost_day, 
                            percent_day = excluded.percent_day;''',
                           (product_list[i][0], product_list[i][1],
                            product_list[i][2], product_list[i][3],
                            product_list[i][4], product_list[i][5],
                            product_list[i][6], product_list[i][7]))
        connection.commit()

        # Сохраняем изменения и закрываем соединение
        connection.commit()
        connection.close()


from pydantic import BaseModel


class Symbol(BaseModel):
    ticker: str
    showName: str


class Price(BaseModel):
    value: float


class LotPrice(BaseModel):
    value: float


class Absolute(BaseModel):
    value: float


class Earnings(BaseModel):
    absolute: Absolute
    relative: float


class EarningsInfo(BaseModel):
    relative: float
    absolute: Absolute


class HistoricalPrices(BaseModel):
    earningsInfo: EarningsInfo


class Product(BaseModel):
    price: Price
    symbol: Symbol
    lotPrice: LotPrice
    historicalPrices: list[HistoricalPrices]
    earnings: Earnings


class Values(BaseModel):
    values: list[Product]

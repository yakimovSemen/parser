import asyncio
import logging
import types

from select_data import Select_Data
from settings import Settings
from parses.stocks_parse import Stocks_Parse
from aiogram import Bot, Dispatcher
from aiogram import F
from aiogram.filters.command import Command
from aiogram.types import Message, InlineKeyboardButton, InlineKeyboardMarkup, CallbackQuery

logging.basicConfig(level=logging.INFO)
bot = Bot(token="6650420959:AAHXX23b1Ihq1_5P2J9n1vRwZLl1dI_n7aU")
dp = Dispatcher()
# кнопки для команды start
update_button = InlineKeyboardMarkup(
    inline_keyboard=[[InlineKeyboardButton(text='Обновить данные', callback_data="button_pressed")],
                     [InlineKeyboardButton(text='Тинькофф инвестиции', url='https://www.tinkoff.ru/invest/')],
                     [InlineKeyboardButton(text='Показать название акций', callback_data="open_data")]])


@dp.message(Command("start"))
async def cmd_start(message: Message, call: CallbackQuery = None):
    await message.answer(f"Приветствую, это бот подключенный к парсеру! "
                         f" Напиши название компании: ", reply_markup=update_button)


# вывод данных по акции, просто текстом
@dp.message(F.text)
async def on_text_message(message: Message):
    await message.answer(f"Акция: {message.text}")
    records = Select_Data.select_stocks(message.text)
    if records:
        for row in records:
            stocks_url = InlineKeyboardMarkup(inline_keyboard=[
                [InlineKeyboardButton(text=f'{row[1]}', url=f'https://www.tinkoff.ru/invest/stocks/{row[0]}')]])
            await message.answer(f"Токен: {row[0]}\n"
                                 f" Название: {row[1]}\n"
                                 f" Цена: {row[2]}\n"
                                 f" Лот: {row[3]}\n"
                                 f" Проценты за год: {row[4]}\n"
                                 f" Проценты за день: {row[7]}\n"
                                 f" Рост цены за год: {row[5]}\n"
                                 f" Рост цены за день: {row[6]}\n",
                                 reply_markup=stocks_url)

    else:
        await message.answer("Такой акции нет в базе(")


# обновление базы
@dp.callback_query(lambda call: call.data == "button_pressed")
async def update_stocks_data(call: CallbackQuery):
    await call.message.answer("Обновляю базу...")
    Stocks_Parse(Settings.url_stocks).parse()
    await call.message.answer("База обновлена!")
    await cmd_start(call.message)


# вывод всех названий акций
@dp.callback_query(lambda call: call.data == "open_data")
async def show_stocks_names(call: CallbackQuery):
    records = Select_Data.all_select_stocks()
    records_format = []
    memory_list = []
    for item in records:
        memory = str(item).replace(',', '').replace('(', '').replace(')', '')  # удаление мусора
        memory_list.append(memory)
        if len(memory_list) == 100:  # 100 - ограничение telegram
            records_format.append(memory_list)
            memory_list = []
    records_format.append(memory_list)
    for i in range(len(records_format)):  # вывод данных
        await call.message.answer("\n".join(records_format[i]))
    await cmd_start(call.message)


async def main():
    await dp.start_polling(bot)


if __name__ == "__main__":
    asyncio.run(main())
